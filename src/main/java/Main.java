import java.util.Random;

public class Main {
    private Main() { }

    private static int maxIterations = 15;

    /**
    Calculator functoinality.
     */
    public static void main() {
        Calculator calculator = new Calculator();
        Random random = new Random();
        
        for (int i = 0; i < maxIterations; i++) {
            int a = random.nextInt();
            int b = random.nextInt();
            System.out.println("Operations with numbers: " + a + " and " + b);
            System.out.println("\tAdd: " + calculator.sum(a, b));
            System.out.println("\tMultiply: " + calculator.mult(a, b));
            System.out.println("\tSubtract: " + calculator.subtract(a, b));
            System.out.println("\tDivide: " + calculator.divide(a, b));
        }
    }
}
