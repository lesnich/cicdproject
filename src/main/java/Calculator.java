public class Calculator {

    /**
     * calculate sum of two numbers.
     * @param a
     * @param b
     */
    public int sum(int a, int b) {
        return a + b;
    }

    /**
     * subtract.
     */
    public int subtract(int a, int b) {
        return a - b;
    }

    /**
     * multiply two numbers.
     */
    public int mult(int a, int b) {
        return a * b;
    }

    /**
     * divide two numbers.
     */
    public int divide(int a, int b) {
        return a / b;
    }
}
