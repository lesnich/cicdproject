
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JUnitTests {

    public static Calculator calculator;

    @BeforeAll
    static void init() {
        calculator = new Calculator();
    }

    @Test
    void testMultiplication () {
        assertEquals(30, calculator.mult(5,6));
        assertEquals(30, calculator.mult(2,15));
    }

    @Test
    void testDivision () {
        assertEquals(1, calculator.divide(6,6));
        assertEquals(0, calculator.divide(2,15));
    }

    @Test
    void testAddition () {
        assertEquals(11, calculator.sum(5,6));
        assertEquals(17, calculator.sum(2,15));
    }

    @Test
    void testSubtraction () {
        assertEquals(-1, calculator.subtract(5,6));
        assertEquals(4, calculator.subtract(19,15));
    }

}
